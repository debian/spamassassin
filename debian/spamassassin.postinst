#!/bin/sh

set -e

if [ "$1" = "configure" ]; then
    # If a new install, or an upgrade from 3.3.2-2 or earlier...
    if ! getent passwd debian-spamd > /dev/null ; then
        adduser --system --group --disabled-login --disabled-password \
                --home /var/lib/spamassassin --no-create-home \
                debian-spamd
    fi

    OWNER=$(stat -c '%U' /var/lib/spamassassin)
    GROUP=$(stat -c '%G' /var/lib/spamassassin)

    # Lets debian-spamd claim ownership of spamassassin updates on upgrades,
    # unless the user has overridden.
    if ! dpkg-statoverride --list /var/lib/spamassassin/* >/dev/null && \
        [ "$OWNER:$GROUP" != "debian-spamd:debian-spamd" ]; then
        chown debian-spamd:debian-spamd /var/lib/spamassassin
        OWNER=debian-spamd
        GROUP=debian-spamd
    fi

    test -d /var/lib/spamassassin/sa-update-keys || \
    install -d -o $OWNER -g $GROUP -m 700 /var/lib/spamassassin/sa-update-keys

    # If gnupg is not available when spamassassin is installed, but
    # is subsequently made available, you'll need to execute
    # `dpkg-reconfigure spamassassin` in order to enable sa-update
    # functionality.
    if command -v gpg > /dev/null; then
        env -i LANG="$LANG" PATH="$PATH" http_proxy="$http_proxy" \
            OWNER="$OWNER" GROUP="$GROUP" \
            runuser -u "$OWNER" --group "$GROUP" -- \
            /usr/bin/sa-update \
            --gpghomedir /var/lib/spamassassin/sa-update-keys \
            --import /usr/share/spamassassin/GPG.KEY
    fi

    # If this is a new installation, copy the rule files to the
    # LOCAL_RULES_DIR.  This avoids Debian bug #739489
    if [ -z "$2" ]; then
        vers=$(perl -MMail::SpamAssassin -e 'print("$Mail::SpamAssassin::VERSION");')
        if [ -n "$vers" ]; then
            dir="/var/lib/spamassassin/$vers"
            if [ ! -f "${dir}/updates_spamassassin_org.cf" ]; then
                rulearchive=$(runuser -u "$OWNER" --group "$GROUP" -- mktemp --tmpdir tmp.XXXXXXXXXX.r000.tgz)
                ( cd /usr/share/spamassassin/ ; runuser -u "$OWNER" --group "$GROUP" -- tar czf $rulearchive * )
                sha512sum "$rulearchive" > "${rulearchive}.sha512"
                runuser -u "$OWNER" --group "$GROUP" -- sa-update --nogpg --install "$rulearchive"
                rm -f "$rulearchive" "${rulearchive}.sha512"
            fi
        fi
    fi
fi

#DEBHELPER#

# Preserve the CRON=1 setting from /etc/default/spamassassin as we
# migrate to systemd timers.
if [ "$1" = "configure" ] || [ "$1" = "abort-upgrade" ] || [ "$1" = "abort-deconfigure" ] || [ "$1" = "abort-remove" ] ; then
    if [ -n "$2" ] &&
           dpkg --compare-versions "$2" lt 4.0.0~0.0svn1879217-1; then
        CRON=0
        [ -f /etc/default/spamassassin ] && . /etc/default/spamassassin
        if [ "$CRON" != 0 ]; then
            deb-systemd-helper unmask 'spamassassin-maintenance.timer' || true
            deb-systemd-helper reenable 'spamassassin-maintenance.timer' || true
        fi
    fi
fi
